//
//  PlantStatusViewController.h
//  relayrPlantPot
//
//  Created by Klingbeil, Thomas on 9/12/14.
//  Copyright (c) 2014 Klingbeil, Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlantStatusViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *plantImage;
@property (weak, nonatomic) IBOutlet UILabel *plantNameLabel;

- (IBAction)emotionSelected:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *emotion1;
@property (weak, nonatomic) IBOutlet UIButton *emotion2;
@property (weak, nonatomic) IBOutlet UIButton *emotion3;
@property (weak, nonatomic) IBOutlet UIButton *emotion4;
@end
