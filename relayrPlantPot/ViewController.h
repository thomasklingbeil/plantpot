//
//  ViewController.h
//  relayrPlantPot
//
//  Created by Klingbeil, Thomas on 9/12/14.
//  Copyright (c) 2014 Klingbeil, Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebConnection.h"
#import "WebConnectionDelegate.h"

@interface ViewController : UIViewController <WebConnectionDelegate>
{
    WebConnection *_connection;
}

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
- (IBAction)checkMailAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *outputTextField;

@end
