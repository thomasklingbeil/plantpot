//
//  ViewController.m
//  relayrPlantPot
//
//  Created by Klingbeil, Thomas on 9/12/14.
//  Copyright (c) 2014 Klingbeil, Thomas. All rights reserved.
//

#import "ViewController.h"
#import "NXOAuth2.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setup {
    [[NXOAuth2AccountStore sharedStore] requestAccessToAccountWithType:@"relayr"];
}

- (IBAction)checkMailAction:(id)sender {
    [self setup];
    
    NSLog([_emailTextField text]);
    if (!_connection) {
        _connection = [[WebConnection alloc] init];
        [_connection setDelegate:self];
    }
    NSString *email = [[_emailTextField text] stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    email = [email stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    [_connection checkEmailValidity:email];
}

#pragma mark - Web Connection Delegate

- (void) finishedRequest:(NSData *)response {
    NSLog(@"Something was returned");
    NSError *error;
    NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
    NSString *message = [responseDict objectForKey:@"message"];
    [_outputTextField setText:message];
}
@end
