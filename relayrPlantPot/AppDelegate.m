//
//  AppDelegate.m
//  relayrPlantPot
//
//  Created by Klingbeil, Thomas on 9/12/14.
//  Copyright (c) 2014 Klingbeil, Thomas. All rights reserved.
//

#import "AppDelegate.h"
#import "NXOAuth2.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    /*
    [[NXOAuth2AccountStore sharedStore] setClientID:@"O3jNiGEpCLyJlIJ-O2P33Mt3xrN1GNR3"
                                             secret:@"ujk6OlnIjrTfA.HdGpz7N8Oeqe1CKyGk"
                                   authorizationURL:[NSURL URLWithString:@"https://api.relayr.io/oauth2/auth"]
                                           tokenURL:[NSURL URLWithString:@"https://api.relayr.io/oauth2/token"]
                                        redirectURL:[NSURL URLWithString:@"http://www.sap.com"]
                                     forAccountType:@"access-own-user-info"];
    */
    [[NXOAuth2AccountStore sharedStore] setClientID:@"O3jNiGEpCLyJlIJ-O2P33Mt3xrN1GNR3"
                                             secret:@"ujk6OlnIjrTfA.HdGpz7N8Oeqe1CKyGk"
                                              scope:[NSSet setWithObject:@"access-own-user-info" ]
                                   authorizationURL:[NSURL URLWithString:@"https://api.relayr.io/oauth2/auth"]
                                           tokenURL:[NSURL URLWithString:@"https://api.relayr.io/oauth2/token"]
                                        redirectURL:[NSURL URLWithString:@"http://www.sap.com"]
                                      keyChainGroup:@""
                                     forAccountType:@"relayr"];
    
   /* [NXOAuth2AccountStore sharedStore] setClientID:<#(NSString *)#> secret:<#(NSString *)#> scope:<#(NSSet *)#> authorizationURL:<#(NSURL *)#> tokenURL:<#(NSURL *)#> redirectURL:<#(NSURL *)#> keyChainGroup:<#(NSString *)#> tokenType:@"Bearer" forAccountType:<#(NSString *)#>
*/    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
