//
//  PlantStatusViewController.m
//  relayrPlantPot
//
//  Created by Klingbeil, Thomas on 9/12/14.
//  Copyright (c) 2014 Klingbeil, Thomas. All rights reserved.
//

#import "PlantStatusViewController.h"

@interface PlantStatusViewController ()

@end

@implementation PlantStatusViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_plantImage setImage:[UIImage imageNamed:@"Foto.JPG"]];
    [_plantNameLabel setText:@"Calathea"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)emotionSelected:(id)sender {
    [_emotion1 setAlpha:0.3];
    [_emotion2 setAlpha:0.3];
    [_emotion3 setAlpha:0.3];
    [_emotion4 setAlpha:0.3];
    
    [sender setAlpha:1.0];
}
@end
