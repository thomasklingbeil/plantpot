//
//  WebConnectionProtocol.h
//
//  Created by Klingbeil, Thomas on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol WebConnectionDelegate <NSObject>

@required

- (void) finishedRequest: (NSData *) response;

@end
