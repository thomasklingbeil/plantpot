//
//  WebConnection.m
//
//  Created by Klingbeil, Thomas on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WebConnection.h"

@implementation WebConnection

@synthesize delegate;

- (void) makeRequestToURL: (NSString *) suffix
{
    NSString *baseURL = @"https://api.relayr.io";//[[NSUserDefaults standardUserDefaults] valueForKey:@"_baseURL"];
    _receivedData = [NSMutableData data];
    NSString *url = [NSString stringWithFormat:@"%@/%@", baseURL, suffix];
    NSLog(url);

    //NSLog(@"Request to %@", url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    //NSString *authToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"_authToken"];
    //[request addValue: authToken forHTTPHeaderField:@"auth"];
    _connection = [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    if ([challenge previousFailureCount] == 0) {
        NSLog(@"received authentication challenge");
        NSURLCredential *newCredential = [NSURLCredential credentialWithUser:[[NSUserDefaults standardUserDefaults] valueForKey:@"_httpUsername"]
                                                                    password:[[NSUserDefaults standardUserDefaults] valueForKey:@"_httpPassword"]
                                                                 persistence:NSURLCredentialPersistenceForSession];
        NSLog(@"credential created");
        [[challenge sender] useCredential:newCredential forAuthenticationChallenge:challenge];
        NSLog(@"responded to authentication challenge");
    }
    else {
        NSLog(@"previous authentication failure");
    }
}

- (void) checkEmailValidity: (NSString *) emailAddress
{
    NSString *url = [NSString stringWithFormat:@"users/validate?email=%@", emailAddress];
    [self makeRequestToURL:url];
}


- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_receivedData appendData:data];
}

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if ([response isKindOfClass:[NSHTTPURLResponse class]])
    {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSInteger status = [httpResponse statusCode];
        
        if (status != 200 && status != 201)
        {
            // request failed...
        }
    }
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    [delegate finishedRequest:_receivedData];
}

@end
