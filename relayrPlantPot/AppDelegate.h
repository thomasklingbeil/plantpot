//
//  AppDelegate.h
//  relayrPlantPot
//
//  Created by Klingbeil, Thomas on 9/12/14.
//  Copyright (c) 2014 Klingbeil, Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
