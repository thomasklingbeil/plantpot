//
//  WebConnection.h
//
//  Created by Klingbeil, Thomas on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebConnectionDelegate.h"

@interface WebConnection : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate>
{
    NSURLConnection *_connection;
    NSMutableData *_receivedData;
}

@property (strong, nonatomic) id<WebConnectionDelegate> delegate;

- (void) checkEmailValidity: (NSString *) emailAddress;

@end
